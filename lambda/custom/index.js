/* eslint-disable  func-names */
/* eslint-disable  no-console */

const Alexa = require('ask-sdk-core');

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
  },
  handle(handlerInput) {
    const speechText = "What up!";
    console.log("inside of the launch handler");
    console.log("Context");
    console.log(JSON.stringify(handlerInput.requestEnvelope.context));

    const resp = handlerInput.responseBuilder
    .speak(speechText)
    .addDirective({
      type: "Alexa.Presentation.APL.RenderDocument",
      token: "doc",
      document: {
        type: "APL",
        version: "1.0",
        mainTemplate: {
          parameters: ["payload"],
          items: [
            {
              type: "Container",
              items: [
                {
                  type: "Text",
                  text: "Rendered!",
                },
                {
                  type: "Video",
                  // source: "http://techslides.com/demos/sample-videos/small.mp4",
                  // source: "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
                  source: "https://scr-testin g.s3.amazonaws.com/b0e5fff8-e740-40ba-9e6f-3fe09e0d6432/teststream/output/master.m3u8",
                  width: "100vw",
                  height: "100vh",
                  id: "video_id",
                  autoplay: true
                }
              ]
            }
          ]
        }
      }
    })
    .getResponse();
    console.log(JSON.stringify(resp))
    return resp
  }
};

const HelpIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    const speechText = 'You can say hello to me!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
        || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    const speechText = 'Goodbye!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    const { request } = handlerInput.requestEnvelope;
    console.log(`Session ended with reason: ${request.reason}`);
    console.log(JSON.stringify(Alexa.getSupportedInterfaces(handlerInput.requestEnvelope)));

    if (request.reason === "ERROR") console.log(request.error.message)

    return handlerInput.responseBuilder.getResponse();
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak('Sorry, I can\'t understand the command. Please say again.')
      .reprompt('Sorry, I can\'t understand the command. Please say again.')
      .getResponse();
  },
};

const skillBuilder = Alexa.SkillBuilders.custom();

exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    SessionEndedRequestHandler
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();
